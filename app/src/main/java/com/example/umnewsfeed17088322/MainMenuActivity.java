//17088322
//NUR NAZURAH AQILAH MOHD ASFAN

package com.example.umnewsfeed17088322;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        Button btn1 = (Button) findViewById(R.id.gotoNewsFeedButton);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, ViewNewsFeedActivity.class));
            }
        });

        Button btn2 = (Button) findViewById(R.id.gotoFeedbackButton);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainMenuActivity.this, SubmitFeedbackActivity.class));
            }
        });
    }
}