//17088322
//NUR NAZURAH AQILAH MOHD ASFAN

package com.example.umnewsfeed17088322;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Rating;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

public class SubmitFeedbackActivity extends AppCompatActivity {
    String[] items = {"UI Interface", "Navigation", "Language", "Others"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_feedback);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner1.setAdapter(adapter);

        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner2.setAdapter(adapter);

        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubmitFeedbackActivity.this, VerifyFeedbackActivity.class);

                String good = spinner1.getSelectedItem().toString();
                String bad = spinner2.getSelectedItem().toString();

                intent.putExtra("good_review", good);
                intent.putExtra("bad_review", bad);
                intent.putExtra("rating", String.valueOf(ratingBar.getRating()));

                startActivity(intent);
            }
        });


    }
}