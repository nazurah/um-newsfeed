//17088322
//NUR NAZURAH AQILAH MOHD ASFAN

package com.example.umnewsfeed17088322;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String username = "17088322";
    String password = "ot2020";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.buttonLogin);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText textUsername = (EditText) findViewById(R.id.editTextUsername);
                EditText textPassword = (EditText) findViewById(R.id.editTextPassword);

                if (textUsername.getText().toString().contains(username) && textPassword.getText().toString().contains(password)) {
                    startActivity(new Intent(MainActivity.this, MainMenuActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, "Wrong username or password. Please reenter the correct one", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}