//17088322
//NUR NAZURAH AQILAH MOHD ASFAN

package com.example.umnewsfeed17088322;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class VerifyFeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_feedback);

        Intent intent = getIntent();

        String good_review = intent.getStringExtra("good_review");
        String bad_review = intent.getStringExtra("bad_review");
        String rating = intent.getStringExtra("rating");

        TextView goodReviewText = (TextView) findViewById(R.id.goodReviewText);
        goodReviewText.setText("You like our [" + good_review + "]");

        TextView badReviewText = (TextView) findViewById(R.id.badReviewText);
        badReviewText.setText("There is still room of improvement for our [" + bad_review + "].");

        TextView ratingText = (TextView) findViewById(R.id.ratingText);
        ratingText.setText("Overall performance rate: [" + rating + "] points");


        Button btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(VerifyFeedbackActivity.this, "Thank you for your feedback and support.", Toast.LENGTH_SHORT).show();
            }
        });

        Button btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}