//17088322
//NUR NAZURAH AQILAH MOHD ASFAN

package com.example.umnewsfeed17088322;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ViewNewsFeedActivity extends AppCompatActivity {
    String[] news = {
            "UM Scientists Discover First Ancient Elephant Fossil In Malaysia",
            "UM Lauds Life On Venus Discovery, Leads Malaysia In Astronomy Through MoU With EAO",
            "UM And Yale University To Establish A Research & Training Centre On HIV Implementation Science",
            "UM Researchers Develop CoSMoS: Monitoring System For Hospital Covid19 Management",
            "UM Releases Joint Report With Clarivate To Evaluate Its Research And Innovation Footprint"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_news_feed);

        ListView list = (ListView) findViewById(R.id.list);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, news);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) list.getItemAtPosition(position);
                TextView selectedNews = (TextView) findViewById(R.id.selectedNewsFeedText);

                selectedNews.setText(selectedItem);
            }
        });

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ViewNewsFeedActivity.this, MainMenuActivity.class));
            }
        });
    }
}